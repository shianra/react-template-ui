export const LOADING_TRUE = '[LOADING] TRUE';
export const LOADING_FALSE = '[LOADING] FALSE';

export function loadingStart() {
  return {
    type: LOADING_TRUE
  };
}

export function loadingStop() {
  return {
    type: LOADING_FALSE
  };
}
