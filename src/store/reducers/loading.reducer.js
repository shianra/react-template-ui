import * as Actions from 'store/actions';

const initialState = {
  loading: false
};

const loading = (state = initialState, action) => {
  switch (action.type) {
    case Actions.LOADING_TRUE: {
      return {
        loading: true
      };
    }
    case Actions.LOADING_FALSE: {
      return {
        loading: false
      };
    }
    default: {
      return state;
    }
  }
};

export default loading;
