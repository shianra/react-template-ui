import { combineReducers } from 'redux';

import auth from 'auth/store/reducers';

import loadingReducer from 'store/reducers/loading.reducer';
import messageReducer from 'store/reducers/message.reducer';
import usersReducer from 'store/reducers/users.reducer';

const appReducer = combineReducers({
  auth,
  loading: loadingReducer,
  message: messageReducer,
  users: usersReducer
})

const rootReducer = (state, action) => {
  return appReducer(state, action);
}

export default rootReducer;