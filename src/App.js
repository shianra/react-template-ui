import history from '@history';
import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';

import PrivateRoute from './PrivateRoute';
import Private from 'containers/Private/Private';
import Public from 'containers/Public/Public';

const App = () => {
  return <Router history={history}>
    <Switch>
      <Route path="/" component={Public} />
    </Switch>
  </Router>
}

export default App;