import FuseUtils from '@core/Utils';
import axios from 'store/actions/axios-config';
import jwtDecode from 'jwt-decode';
/* eslint-disable camelcase */

class JwtService extends FuseUtils.EventEmitter {
	init() {
		this.setInterceptors();
		this.handleAuthentication();
	}

	setInterceptors = () => {
		axios.interceptors.response.use(
			response => {
				return response;
			},
			err => {
				return new Promise((resolve, reject) => {
					if (err.response.status === 401 && err.config && !err.config.__isRetryRequest) {
						// if you ever get an unauthorized response, logout the user
						this.emit('onAutoLogout', 'Invalid token');
						this.setSession(null);
					}
					throw err;
				});
			}
		);
	};

	handleAuthentication = () => {
		const token = this.getAccessToken();

		if (!token) {
			this.emit('onNoAccessToken');
			return;
		}

		if (this.isAuthTokenValid(token)) {
			this.setSession(token);
			this.emit('onAutoLogin', true);
		} else {
			this.setSession(null);
			this.emit('onAutoLogout', 'token expired');
		}
	};

	createUser = data => {
		return new Promise((resolve, reject) => {
			axios.post('/api/users', data)
				.then(response => {
					if (response.data.message) {
						resolve(response.data.message);
					} else {
						reject(response.data.error);
					}
				})
				.catch(error => {
					reject({ response: error.response });
				});
		});
	};

	/**
	 * @name                signInWithLoginNameAndPassword
	 * @description         Authorise a user using form data
	 * @param   {object}    values: { loginName, password }
	 * @param   {function}  history
	*/
	signInWithLoginNameAndPassword = (loginName, password) => {
		return new Promise((resolve, reject) => {
			axios
				.post('/api/users/login', {
					loginName,
					password
				})
				.then(response => {
					if (response.data.token && response.data.userInfo) {
						this.setSession(response.data.token, response.data.nodeToken);
						resolve(response.data.userInfo);
					} else {
						reject(response.data.error);
					}
				})
				.catch(error => {
					reject({
						response: {
							data: {
								errors: [
									{
										message: 'Your login details are incorrect.'
									}
								]
							}
						}
					});
				});
		});
	};

	signInWithToken = () => {
		return new Promise((resolve, reject) => {
			axios
				.get('/api/users/login', {
					data: {
						token: this.getAccessToken()
					}
				})
				.then(response => {
					if (response.data.user) {
						this.setSession(response.data.token, response.data.nodeToken);
						instance.defaults.headers.common['Authorization'] = response.data.token;
						instance.defaults.headers.common['nodeAuth'] = response.data.nodeToken;
						resolve(response.data.user);
					} else {
						this.logout();
						Promise.reject(new Error('Failed to login with token.'));
					}
				})
				.catch(error => {
					this.logout();
					Promise.reject(new Error('Failed to login with token.'));
				});
		});
	};

	updateUserData = user => {
		return axios.post('/api/auth/user/update', {
			user
		});
	};

	setSession = (token, nodeAuth) => {
		if (token && nodeAuth) {
			localStorage.setItem('jwt_token', token);
			axios.defaults.headers.common['Authorization'] = token;
			localStorage.setItem('node_token', nodeAuth);
			axios.defaults.headers.common['nodeAuth'] = nodeAuth;
		} else {
			localStorage.removeItem('jwt_token');
			delete axios.defaults.headers.common['Authorization'];
			localStorage.removeItem('node_token');
			delete axios.defaults.headers.common['nodeAuth'];
		}
	};

	logout = () => {
		this.setSession(null);
	};

	isAuthTokenValid = token => {
		if (!token) {
			return false;
		}
		const decoded = jwtDecode(token);
		const currentTime = Date.now() / 1000;
		if (decoded.exp < currentTime) {
			console.warn('access token expired');
			return false;
		}

		return true;
	};

	getAccessToken = () => {
		return window.localStorage.getItem('jwt_token');
	};
}

const instance = new JwtService();

export default instance;
