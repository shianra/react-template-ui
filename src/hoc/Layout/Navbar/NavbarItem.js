import React from 'react';
import { NavLink } from 'react-router-dom';

const NavbarItem = ({ item: { externalLink, linkName, linkUrl } }) => {
  if (externalLink) {
    return <a className="block lg:inline-block mx-4" href={linkUrl} target="_blank">{linkName} <i className="far fa-external-link"></i></a>
  } else {
    return <NavLink activeClassName="font-bold" className="block lg:inline-block mx-4" to={linkUrl} exact>{linkName}</NavLink>
  }
}

export default NavbarItem;
