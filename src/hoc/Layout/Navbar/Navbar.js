import React from 'react';

import NavbarItem from './NavbarItem';
import { Link } from 'react-router-dom';

const Navbar = ({ user, isAuthenticated }) => {
  const navItemsLeft = [
    {
      authRequired: false,
      externalLink: false,
      linkName: 'Home',
      linkUrl: '/',
      id: 'nbhome'
    }, {
      authRequired: true,
      externalLink: false,
      linkName: 'View Users',
      linkUrl: '/admin/users',
      id: 'nbusers'
    }
  ];
  const navItemsRight = [
    {
      authRequired: true,
      externalLink: false,
      linkName: user.name,
      linkUrl: `/admin/users/${user.id}`,
      id: 'nbr01'
    }, {
      authRequired: true,
      externalLink: false,
      linkName: 'Logout',
      linkUrl: '/logout',
      id: 'nbr02'
    }, {
      authRequired: false,
      externalLink: false,
      linkName: 'Login',
      linkUrl: '/login',
      id: 'nbr03'
    }, {
      authRequired: false,
      externalLink: false,
      linkName: 'Register',
      linkUrl: '/register',
      id: 'nbr04'
    }
  ];

  /**
   * TODO: Responsive burger menu still needs to be implemented.
   */

  return <div className="container mx-auto flex items-end justify-between flex-wrap p-6">
    <div className="flex flex-shrink-0 mr-12">
      <Link className="no-underline hover:no-underline font-bold text-2xl lg:text-4xl" to="/">React Template</Link>
    </div>
    <div className="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
      <div className="lg:flex-grow py-2">
        {navItemsLeft.map(navItem => {
          if (navItem.authRequired) {
            if (isAuthenticated) {
              return <NavbarItem item={navItem} key={navItem.id} />;
            }
          } else {
            return <NavbarItem item={navItem} key={navItem.id} />;
          }
        })}
      </div>
      <div className="py-2">
        {navItemsRight.map(navItem => {
          if (navItem.authRequired) {
            if (isAuthenticated) {
              return <NavbarItem item={navItem} key={navItem.id} />;
            }
          } else {
            return <NavbarItem item={navItem} key={navItem.id} />;
          }
        })}
      </div>
    </div>
  </div>
}

export default Navbar;
