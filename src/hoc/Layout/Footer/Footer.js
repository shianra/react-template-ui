import React from 'react';

const Footer = () => {
    return <footer className="bg-dark-700">
        <div className="container mx-auto px-8">

            <div className="w-full flex flex-col md:flex-row py-20">
                <div className="mr-12">
                    <a className="no-underline hover:no-underline font-bold text-2xl lg:text-4xl" href="#">React Template</a>
                </div>
                <div className="">
                    <p>React Template by <span className="font-semibold">Sha</span>.</p>
                    <p>Stock photo by <a href="https://unsplash.com/@creativeexchange" target="_blank">The Creative Exchange</a> on <a href="https://unsplash.com" target="_blank">Unsplash</a>.</p>
                </div>
            </div>

        </div>
    </footer>
}

export default Footer;
