import Home from 'containers/Private/Home/Home';

/**
 * User containers
 */
import UserDetails from 'containers/Private/User/Details/Details';
import UserDelete from 'containers/Private/User/Delete/Delete';
import UserEdit from 'containers/Private/User/Edit/Edit';
import UserList from 'containers/Private/User/List/List';

export const routes = [
  {
    component: Home,
    exact: true,
    id: 'private-home-01',
    path: '/admin'
  },
  /**
   * User routes
   */
  {
    component: UserDetails,
    exact: true,
    id: 'private-user-details-01',
    path: '/admin/users/:userId'
  }, {
    component: UserDelete,
    exact: true,
    id: 'private-user-delete-01',
    path: '/admin/users/:userId/delete'
  }, {
    component: UserEdit,
    exact: true,
    id: 'private-user-edit-01',
    path: '/admin/users/:userId/edit'
  }, {
    component: UserList,
    exact: true,
    id: 'private-user-list-01',
    path: '/admin/users'
  }
]