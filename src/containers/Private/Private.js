import React from 'react';
import { useSelector } from 'react-redux';
import { Route } from 'react-router-dom';

/**
 * We can use a completely different set of routes for this area if we wish. There is a
 * section in the README file, titled "Routing with react-router-dom". Please refer to this
 * section for more information.
 */
import { routes } from 'routes/private';

import Header from 'hoc/Layout/Header/Header';
import Footer from 'hoc/Layout/Footer/Footer';

function Private(props) {
  const user = useSelector(({ auth }) => auth.user);

  return <div>
    <div className="main-content">
      <Header isAuthenticated={user.isAuthenticated} user={user.data} />
      <section className="section">
        <div className="container">
          {routes.map(route => {
            return <Route
              key={route.id}
              path={route.path}
              exact={route.exact}
              component={route.component}
            />
          })}
        </div>
      </section>
    </div>
    <Footer />
  </div>
}

export default Private;