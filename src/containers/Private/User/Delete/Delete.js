import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from '@lodash';

import Button from '@material-ui/core/Button';

import { deleteUser, getUser } from 'store/actions';

import Alert from 'components/Alert';

class UserDelete extends Component {
  componentDidMount() {
    const { getUser, match: { params: { userId } } } = this.props;
    getUser(userId);
  }

  render() {
    const { deleteUser, message, user: { details } } = this.props;

    if (!_.isEmpty(message.text)) {
      return <Alert type={message.type}>{message.text}</Alert>
    }

    return <Alert type="">
      <p>Are you sure you want to delete <strong>{details.name}</strong> ({details.email})?</p>

      <div className="field is-grouped is-grouped-right">
        <div className="control">
          <Link className="is-text" to="/admin/users">Cancel</Link>
        </div>
        <div className="control">
          <Button className="is-danger" clickEvent={() => deleteUser(details.id)}>Delete</Button>
        </div>
      </div>
    </Alert>
  }
}

const mapStateToProps = state => ({
  message: state.message,
  user: state.auth.user
})

export default connect(mapStateToProps, { deleteUser, getUser })(UserDelete);